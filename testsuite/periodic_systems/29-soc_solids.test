# -*- coding: utf-8 mode: shell-script -*-

Test       : SOC in periodic systems
Program    : octopus
TestGroups : long-run, periodic_systems
Enabled    : Yes

Input      : 29-soc_solids.01-gs.inp

match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1

match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 3
match ; Space group        ; GREPFIELD(out, 'Space group', 4) ; 229
match ; No. of symmetries  ; GREPFIELD(out, 'symmetries that can be used', 5)  ;  48

Precision: 1.00e-06
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -5.36258581
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -6.56757813
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; 0.80842195
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.06384959
match ;   Exchange energy      ; GREPFIELD(static/info, 'Exchange    =', 3) ; -1.27908063
match ;   Correlation energy   ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.2358683
match ;   Kinetic energy       ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 2.30064936
match ;   External energy      ; GREPFIELD(static/info, 'External    =', 3) ; 0.3554423

Precision: 1.00e-05
match ;  Eigenvalue  1  ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.264487
match ;  Eigenvalue  2  ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -0.264487
match ;  Eigenvalue  4  ; GREPFIELD(static/info, '#k =       1', 3, 4) ;  0.584524
match ;  Eigenvalue  5  ; GREPFIELD(static/info, '#k =       1', 3, 5) ;  0.763266

# Testing the commutator [r,V_NL] through the current
Precision: 7.33e-14
match ;     x current sp 1    ; LINEFIELD(static/current-sp1-x.x\=0\,y\=0, 5, 2) ; -0.00146598997599441
Precision: 7.31e-14
match ;     x current sp 2    ; LINEFIELD(static/current-sp2-x.x\=0\,y\=0, 5, 2) ; 0.0014659899759947002
Precision: 2.53e-14
match ;     x current sp 3    ; LINEFIELD(static/current-sp3-x.x\=0\,y\=0, 5, 2) ; -0.0038665807747396003
Precision: 6.08e-14
match ;     x current sp 4    ; LINEFIELD(static/current-sp4-x.x\=0\,y\=0, 5, 2) ; 0.003858136128546726

# Testing the forces
# Tolerances are too high for been really meaningful
Precision: 7.06e-12
match ;    z Force Local      ; LINEFIELD(static/forces, 2, 14) ; -1.88269e-15
Precision: 1.04e-11
match ;    z Force NL         ; LINEFIELD(static/forces, 2, 17) ; -1.4830635e-14

Precision: 5.1e-05
match ;  Total Magnetic Moment - x ; GREPFIELD(static/info, 'mx = ', 3) ; 0.000000
match ;  Total Magnetic Moment - y ; GREPFIELD(static/info, 'my = ', 6) ; 0.000000
match ;  Total Magnetic Moment - z ; GREPFIELD(static/info, 'mz = ', 9) ; 0.000000


